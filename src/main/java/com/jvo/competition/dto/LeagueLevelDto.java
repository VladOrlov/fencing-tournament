package com.jvo.competition.dto;

import lombok.Data;

@Data
public class LeagueLevelDto {

  private Long id;

  private String name;

}
