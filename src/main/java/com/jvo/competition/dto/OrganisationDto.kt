package com.jvo.competition.dto

import com.neovisionaries.i18n.CountryCode

data class OrganisationDto(
        var id: String = "",
        var name: String = "",
        var countryCode: CountryCode = CountryCode.UNDEFINED,
        var countryName: String = ""
)