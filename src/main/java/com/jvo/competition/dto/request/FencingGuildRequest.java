package com.jvo.competition.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FencingGuildRequest {

  @Size(min = 3, max = 120)
  @NotNull
  private String name;

  @NotNull
  private String countryCode;

  @NotNull
  private String countryName;

}
