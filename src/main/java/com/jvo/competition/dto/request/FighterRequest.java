package com.jvo.competition.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FighterRequest {

  @Size(min = 3, max = 25)
  @NotNull
  private String firstName;

  @Size(min = 3, max = 25)
  @NotNull
  private String lastName;

  @Min(16) @Max(60)
  @Positive
  @NotNull
  private Integer age;

  private Long guildId;

}
