package com.jvo.competition.dto.request

import com.neovisionaries.i18n.CountryCode

data class OrganisationRequest(
        val name: String = "",
        val organisationType: String = "",
        val countryCode: CountryCode = CountryCode.UNDEFINED,
        val countryName: String = ""
)