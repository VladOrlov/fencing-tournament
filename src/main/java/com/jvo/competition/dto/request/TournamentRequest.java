package com.jvo.competition.dto.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TournamentRequest {

  @NotNull
  private String name;

  @NotNull
  private String countryCode;

  @NotNull
  private String countryName;

  @NotNull
  private String description;
}
