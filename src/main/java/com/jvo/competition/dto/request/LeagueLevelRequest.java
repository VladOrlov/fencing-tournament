package com.jvo.competition.dto.request;

import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Value
public class LeagueLevelRequest {

  @NotNull
  @Size(min = 3, max = 150)
  private String name;

}
