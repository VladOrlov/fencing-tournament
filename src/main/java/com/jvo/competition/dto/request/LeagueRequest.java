package com.jvo.competition.dto.request;

import com.jvo.competition.model.enums.LeagueType;
import com.neovisionaries.i18n.CountryCode;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Value
@Builder
public class LeagueRequest {

  @NotNull
  @Size(min = 3, max = 150)
  private String name;

  private CountryCode countryCode;

  private String countryName;

  @NotNull
  @Positive
  private Long level;

  @NotNull
  //@Size(min = 2, max = 20)
  private LeagueType leagueType;

}
