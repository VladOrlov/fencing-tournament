package com.jvo.competition.dto;

import com.neovisionaries.i18n.CountryCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TournamentDto {

  private Long id;

  private String name;

  private CountryCode countryCode;

  private String countryName;

  private String description;

}
