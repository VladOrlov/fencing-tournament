package com.jvo.competition.dto;

import lombok.Data;

@Data
public class LeagueDto {

  private Long id;

  private String name;

  private String countryCode;

  private String countryName;

  private LeagueLevelDto level;

}
