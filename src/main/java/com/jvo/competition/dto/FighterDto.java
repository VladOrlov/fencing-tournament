package com.jvo.competition.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FighterDto {

  private Long id;

  private String firstName;

  private String lastName;

  private Integer age;

  private FencingGuildDto guild;

}
