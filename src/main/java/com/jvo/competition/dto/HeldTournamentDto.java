package com.jvo.competition.dto;

import com.jvo.competition.model.Tournament;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HeldTournamentDto {

  private Long id;

  private Tournament tournament;

  private LocalDateTime startDate;

  private LocalDateTime endDate;

  private String address;

}
