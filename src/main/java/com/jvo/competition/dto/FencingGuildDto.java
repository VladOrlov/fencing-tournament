package com.jvo.competition.dto;

import com.neovisionaries.i18n.CountryCode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FencingGuildDto {

  private Long id;

  private String name;

  private CountryCode countryCode;

  private String countryName;

  @Singular
  private List<FighterDto> fighters;

}
