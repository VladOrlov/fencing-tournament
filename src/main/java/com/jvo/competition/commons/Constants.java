package com.jvo.competition.commons;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

  @UtilityClass
  public static class WebConstants {
    public static final String BASE_URI = "/api";
    public static final String VERSION = "v1";
    public static final String BASE_VERSION_URI = BASE_URI + "/" + VERSION;
    public static final String ORGANISATION_URI = BASE_VERSION_URI + "/organisations";
    public static final String FENCING_GUILD_URI = BASE_VERSION_URI + "/fencing-guilds";
    public static final String LEAGUE_URI = BASE_VERSION_URI + "/leagues";
    public static final String LEAGUE_LEVEL_URI = BASE_VERSION_URI + "/league-levels";
    public static final String FIGHTER_URI = BASE_VERSION_URI + "/fighters";
    public static final String TOURNAMENT_URI = BASE_VERSION_URI + "/tournaments";
    public static final String HELD_TOURNAMENT_URI = TOURNAMENT_URI + "/held-tournaments";
  }
}
