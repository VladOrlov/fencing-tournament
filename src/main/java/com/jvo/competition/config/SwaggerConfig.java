package com.jvo.competition.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.schema.AlternateTypeRules.newRule;


@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    private static final String TOKEN = "x-auth-token";

    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
//    private final TypeResolver typeResolver;
//
//
//    /**
//     * Bean initializer.
//     *
//     * @param typeResolver - typeResolver variable
//     */
//    @Autowired
//    public SwaggerConfig(TypeResolver typeResolver) {
//        this.typeResolver = typeResolver;
//    }

//    @Bean
//    public Docket api() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.jvo.competition.controller.sync"))
//                .paths(PathSelectors.any())
//                .build()
//                .pathMapping("/")
//                .enableUrlTemplating(true)
//                .directModelSubstitute(LocalDate.class, String.class)
//                .directModelSubstitute(LocalDateTime.class, Long.class)
//                .genericModelSubstitutes(ResponseEntity.class)
//        .alternateTypeRules(
//            newRule(
//                typeResolver.resolve(
//                    DeferredResult.class,
//                    typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
//                typeResolver.resolve(WildcardType.class)))
//        .useDefaultResponseMessages(false)
//        .globalResponseMessage(
//            RequestMethod.GET,
//            newArrayList(
//                new ResponseMessageBuilder()
//                    .code(500)
//                    .message("500 message")
//                    .responseModel(new ModelRef("Error"))
//                    .build()))
//        .enableUrlTemplating(false)
//        .securitySchemes(newArrayList(apiKey()))
//        .securityContexts(newArrayList(securityContext()))
//                .apiInfo(apiInfo());
//    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Fencing Tournament Api")
                .version("1.0")
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey(TOKEN, TOKEN, "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/anyPath.*"))
                .build();
    }

//  @Bean
//  SecurityConfiguration security() {
//    return new SecurityConfiguration(
//        "test-app-client-id",
//        "test-app-client-secret",
//        "test-app-realm",
//        "Fencing-tournament",
//        "",
//        ApiKeyVehicle.HEADER,
//        TOKEN,
//        ",");
//  }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return newArrayList(new SecurityReference(TOKEN, authorizationScopes));
    }
}
