package com.jvo.competition.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Locale;

/**
 * Application configuration class
 */
@Configuration
public class AppConfig {

  @Bean
  public Locale locale() {
    return new Locale("uk");
  }
}
