package com.jvo.competition.controller.sync

import com.jvo.competition.commons.Constants.WebConstants.ORGANISATION_URI
import com.jvo.competition.dto.OrganisationDto
import com.jvo.competition.dto.request.OrganisationRequest
import com.jvo.competition.model2.Organisation
import com.jvo.competition.service.sync.OrganisationService
import com.neovisionaries.i18n.CountryCode
import org.modelmapper.ModelMapper
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController(value = ORGANISATION_URI)
class OrganisationController(private val organisationService: OrganisationService,
                             private val modelMapper: ModelMapper) {

    @GetMapping
    fun getOrganisations(): ResponseEntity<List<OrganisationDto>> {
        val organisations = organisationService.getAll()
                .map { organisation -> modelMapper.map(organisation.javaClass, OrganisationDto().javaClass) }

        return ResponseEntity.ok(organisations)
    }

    @PostMapping
    fun getOrganisations(@RequestBody organisationRequest: OrganisationRequest): ResponseEntity<OrganisationDto> {
        val organisation = Organisation(
                name = organisationRequest.name,
                organisationType = organisationRequest.organisationType,
                countryCode = CountryCode.UA,
                countryName = "Ukraine")

        return ResponseEntity.ok(organisationService.add(organisation))
    }
}