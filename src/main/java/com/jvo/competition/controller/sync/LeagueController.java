package com.jvo.competition.controller.sync;

import com.jvo.competition.dto.LeagueDto;
import com.jvo.competition.dto.request.LeagueRequest;
import com.jvo.competition.exception.ApiResponseException;
import com.jvo.competition.exception.ErrorMessage;
import com.jvo.competition.service.sync.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import static com.jvo.competition.commons.Constants.WebConstants.LEAGUE_URI;

@RestController
@RequestMapping(path = LEAGUE_URI)
public class LeagueController {

    private final LeagueService leagueService;

    @Autowired
    public LeagueController(LeagueService leagueService) {
        this.leagueService = leagueService;
    }

    @GetMapping
    public ResponseEntity<List<LeagueDto>> getLeagues() {
        return ResponseEntity.ok(leagueService.getAll());
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LeagueDto> getLeagueById(@PathVariable Long id) {
        return ResponseEntity.ok(leagueService.getById(id)
                .orElseThrow(() -> new ApiResponseException(
                        ErrorMessage.of(HttpStatus.NOT_FOUND, "League with id %s not found!"))));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LeagueDto> add(@RequestBody @Validated LeagueRequest leagueRequest) {
        LeagueDto savedLeague = leagueService.add(leagueRequest);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(savedLeague.getId()).toUri();

        return ResponseEntity.created(location).body(savedLeague);
    }

    /**
     * Update existing fighter.
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateLeague(@PathVariable Long leagueId, @RequestBody @Validated LeagueRequest leagueRequest) {
        LeagueDto savedLeague = leagueService.update(leagueRequest);

        if (Objects.isNull(savedLeague)) {
            throw new ApiResponseException(ErrorMessage.of(HttpStatus.INTERNAL_SERVER_ERROR,
                    String.format("Update of league with id %s failed!", leagueId)));
        }
    }

    /**
     * Delete fighter by id.
     */
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(path = "/{id}")
    public void deleteLeague(@PathVariable Long leagueId) {
        if (leagueService.notExist(leagueId)) {
            throw new ApiResponseException(ErrorMessage.of(
                    HttpStatus.NOT_FOUND, String.format("Delete failed! League with id %s not found!", leagueId)));
        }
        leagueService.delete(leagueId);
    }
}
