package com.jvo.competition.controller.sync;

import static com.jvo.competition.commons.Constants.WebConstants.FENCING_GUILD_URI;

import com.neovisionaries.i18n.CountryCode;
import com.jvo.competition.dto.FencingGuildDto;
import com.jvo.competition.dto.request.FencingGuildRequest;
import com.jvo.competition.exception.ApiResponseException;
import com.jvo.competition.exception.ErrorMessage;
import com.jvo.competition.model.FencingGuild;
import com.jvo.competition.service.sync.FencingGuildService;

import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = FENCING_GUILD_URI)
public class FencingGuildController {

  private final FencingGuildService fencingGuildService;

  private final ModelMapper modelMapper;

  @Autowired
  public FencingGuildController(FencingGuildService fencingGuildService, ModelMapper modelMapper) {
    this.fencingGuildService = fencingGuildService;
    this.modelMapper = modelMapper;
  }

  /**
   * Return all available guilds.
   *
   * @return List of FencingGuildDto
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<FencingGuildDto>> getGuilds() {
    return ResponseEntity.ok(fencingGuildService.getAll());
  }

  /**
   * Return guild by given id.
   *
   * @return FencingGuildDto
   */
  @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<FencingGuildDto> getGuildById(@PathVariable Long id) {
    FencingGuildDto fencingGuild = fencingGuildService.getById(id).orElseThrow(() -> new ApiResponseException(
        ErrorMessage.of(HttpStatus.NOT_FOUND, String.format("Fencing guild with id %s not found!", id))));

    return ResponseEntity.ok(fencingGuild);
  }

  /**
   * Add new guild.
   *
   * @return dto of saved FencingGuild
   */
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<FencingGuildDto> addGuild(@RequestBody @Validated FencingGuildRequest fencingGuildRequest) {
    FencingGuild fencingGuild = modelMapper.map(fencingGuildRequest, FencingGuild.class);
    fencingGuild.setCountryCode(extractCountryCode(fencingGuildRequest));
    FencingGuildDto savedFencingGuild = fencingGuildService.add(fencingGuild);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
        .path("/{id}").buildAndExpand(fencingGuild.getId()).toUri();

    return ResponseEntity.created(location).body(savedFencingGuild);
  }

  /**
   * Update existing guild.
   */
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public void updateGuild(@PathVariable("id") Long guildId, @RequestBody @Validated FencingGuildRequest fencingGuildRequest) {
    FencingGuild fencingGuild = modelMapper.map(fencingGuildRequest, FencingGuild.class);
    fencingGuild.setId(guildId);
    fencingGuild.setCountryCode(extractCountryCode(fencingGuildRequest));

    FencingGuildDto savedFencingGuild = fencingGuildService.update(fencingGuild);

    if (Objects.isNull(savedFencingGuild)) {
      throw new ApiResponseException(ErrorMessage.of(HttpStatus.INTERNAL_SERVER_ERROR,
          String.format("Update of fencing guild with id %s failed!", guildId)));
    }
  }

  /**
   * Delete guild by id.
   */
  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping(path = "/{id}")
  public void deleteGuild(@PathVariable Long id) {
    if (fencingGuildService.notExist(id)) {
      throw new ApiResponseException(ErrorMessage.of(
          HttpStatus.NOT_FOUND, String.format("Delete failed! Fencing guild with id %s not found!", id)));
    }
    fencingGuildService.delete(id);
  }

  private CountryCode extractCountryCode(FencingGuildRequest fencingGuildRequest) {
    CountryCode code = CountryCode.getByCode(fencingGuildRequest.getCountryCode(), false);
    if (code == CountryCode.UNDEFINED) {
      List<CountryCode> countryCodesByName = CountryCode.findByName(fencingGuildRequest.getCountryName());
      if (CollectionUtils.isNotEmpty(countryCodesByName)) {
        code = countryCodesByName.get(0);
      }
    }
    return code;
  }
}
