package com.jvo.competition.controller.sync;

import com.jvo.competition.dto.HeldTournamentDto;
import com.jvo.competition.dto.request.HeldTournamentRequest;
import com.jvo.competition.model.HeldTournament;
import com.jvo.competition.service.sync.HeldTournamentService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

import static com.jvo.competition.commons.Constants.WebConstants.HELD_TOURNAMENT_URI;

/**
 * @author gss
 */
@RestController
@RequestMapping(value = HELD_TOURNAMENT_URI)
public class HeldTournamentController {

  private HeldTournamentService heldTournamentService;
  private ModelMapper modelMapper;

  @Autowired
  public HeldTournamentController(HeldTournamentService heldTournamentService, ModelMapper modelMapper) {
    this.heldTournamentService = heldTournamentService;
    this.modelMapper = modelMapper;
  }

  @ApiOperation(value = "Get all holdings for all tournaments tournaments")
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getAllEventsForAllTournaments() {
    return ResponseEntity.ok(heldTournamentService.getAll());
  }

  @ApiOperation(value = "Get all holdings for specific tournament")
  @GetMapping(path = "/tournament/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getAllEventsForTournament(@PathVariable long id) {
    return ResponseEntity.ok(heldTournamentService.getAllForTournament(id));
  }

  @ApiOperation(value = "Get holding by id")
  @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getEventById(@PathVariable long id) {
    return ResponseEntity.ok(heldTournamentService.getById(id));
  }

  @ApiOperation(value = "Create new event")
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> addEvent(@RequestBody @Validated HeldTournamentRequest request) {
    HeldTournamentDto saved = heldTournamentService.add(modelMapper.map(request, HeldTournament.class));
    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
        .path("/{id}").buildAndExpand(saved.getId()).toUri();
    return ResponseEntity.created(location).body(saved);
  }

  @ApiOperation(value = "Update existing event")
  @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> updateEvent(@PathVariable Long id, @RequestBody @Validated HeldTournamentRequest request) {
    return ResponseEntity.ok(heldTournamentService.update(id, modelMapper.map(request, HeldTournament.class)));
  }

  @ApiOperation(value = "Delete event")
  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> deleteEvent(@PathVariable Long id) {
    heldTournamentService.delete(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
