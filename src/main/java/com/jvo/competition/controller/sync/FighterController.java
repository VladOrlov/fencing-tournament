package com.jvo.competition.controller.sync;

import static com.jvo.competition.commons.Constants.WebConstants.FIGHTER_URI;

import com.jvo.competition.exception.ApiResponseException;
import com.jvo.competition.exception.ErrorMessage;
import com.jvo.competition.dto.FighterDto;
import com.jvo.competition.dto.request.FighterRequest;
import com.jvo.competition.model.Fighter;
import com.jvo.competition.service.sync.FighterService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = FIGHTER_URI)
public class FighterController {

  private final FighterService fighterService;

  private final ModelMapper modelMapper;

  @Autowired
  public FighterController(FighterService fighterService, ModelMapper modelMapper) {
    this.fighterService = fighterService;
    this.modelMapper = modelMapper;
  }

  /**
   * Return all existing fighters.
   *
   * @return List of FighterDto
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<FighterDto>> getFighters() {
    return ResponseEntity.ok(fighterService.getAll());
  }

  /**
   * Return fighter by id.
   *
   * @return FighterDto with given id
   */
  @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<FighterDto> getFighterById(@PathVariable Long fighterId) {
    FighterDto fighter = fighterService.getById(fighterId).orElseThrow(() -> new ApiResponseException(
        ErrorMessage.of(HttpStatus.NOT_FOUND, String.format("Fencing guild with id %s not found!", fighterId))));

    return ResponseEntity.ok(fighter);
  }

  /**
   * Add new fighter.
   *
   * @return dto of saved Fighter
   */
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<FighterDto> addFighter(@RequestBody @Validated FighterRequest fighterRequest) {
    Fighter fighter = modelMapper.map(fighterRequest, Fighter.class);
    FighterDto savedFighter = fighterService.add(fighter);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
        .path("/{id}").buildAndExpand(savedFighter.getId()).toUri();

    return ResponseEntity.created(location).body(savedFighter);
  }

  /**
   * Update existing fighter.
   */
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public void updateFighter(@PathVariable("id") Long fighterId, @RequestBody @Validated FighterRequest fighterRequest) {
    Fighter fighter = modelMapper.map(fighterRequest, Fighter.class);

    FighterDto savedFighter = fighterService.update(fighter);

    if (Objects.isNull(savedFighter)) {
      throw new ApiResponseException(ErrorMessage.of(HttpStatus.INTERNAL_SERVER_ERROR,
          String.format("Update of fighter with id %s failed!", fighterId)));
    }
  }

  /**
   * Delete fighter by id.
   */
  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping(path = "/{id}")
  public void deleteFighter(@PathVariable("id") Long fighterId) {
    if (fighterService.notExist(fighterId)) {
      throw new ApiResponseException(ErrorMessage.of(
          HttpStatus.NOT_FOUND, String.format("Delete failed! Fighter with id %s not found!", fighterId)));
    }
    fighterService.delete(fighterId);
  }
}
