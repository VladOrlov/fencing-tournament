package com.jvo.competition.controller.sync;

import com.jvo.competition.exception.ApiResponseException;
import com.jvo.competition.exception.ErrorMessage;
import com.jvo.competition.dto.LeagueLevelDto;
import com.jvo.competition.dto.request.LeagueLevelRequest;
import com.jvo.competition.service.sync.LeagueLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import static com.jvo.competition.commons.Constants.WebConstants.LEAGUE_LEVEL_URI;

@RestController
@RequestMapping(path = LEAGUE_LEVEL_URI)
public class LeagueLevelController {

  private final LeagueLevelService leagueLevelService;

  @Autowired
  public LeagueLevelController(LeagueLevelService leagueLevelService) {
    this.leagueLevelService = leagueLevelService;
  }

  @GetMapping
  public ResponseEntity<List<LeagueLevelDto>> getLeagues() {
    return ResponseEntity.ok(leagueLevelService.getAll());
  }

  @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<LeagueLevelDto> getLeagueById(@PathVariable Long id) {
    return ResponseEntity.ok(leagueLevelService.getById(id)
        .orElseThrow(() -> new ApiResponseException(
            ErrorMessage.of(HttpStatus.NOT_FOUND, "League with id %s not found!"))));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<LeagueLevelDto> add(@RequestBody @Validated LeagueLevelRequest leagueLevelRequest) {
    LeagueLevelDto savedLevelLeague = leagueLevelService.add(leagueLevelRequest);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
        .path("/{id}").buildAndExpand(savedLevelLeague.getId()).toUri();

    return ResponseEntity.created(location).body(savedLevelLeague);
  }

  /**
   * Update existing fighter.
   */
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public void updateLeague(@PathVariable Long leagueId, @RequestBody @Validated LeagueLevelRequest leagueLevelRequest) {
    LeagueLevelDto savedLeague = leagueLevelService.update(leagueLevelRequest);

    if (Objects.isNull(savedLeague)) {
      throw new ApiResponseException(ErrorMessage.of(HttpStatus.INTERNAL_SERVER_ERROR,
          String.format("Update of league with id %s failed!", leagueId)));
    }
  }

  /**
   * Delete fighter by id.
   */
  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping(path = "/{id}")
  public void deleteLeague(@PathVariable Long leagueLevelId) {
    if (leagueLevelService.notExist(leagueLevelId)) {
      throw new ApiResponseException(ErrorMessage.of(
          HttpStatus.NOT_FOUND, String.format("Delete failed! League with id %s not found!", leagueLevelId)));
    }
    leagueLevelService.delete(leagueLevelId);
  }
}
