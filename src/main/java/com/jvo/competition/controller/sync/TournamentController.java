package com.jvo.competition.controller.sync;

import com.jvo.competition.exception.ApiResponseException;
import com.jvo.competition.exception.ErrorMessage;
import com.jvo.competition.dto.TournamentDto;
import com.jvo.competition.dto.request.TournamentRequest;
import com.jvo.competition.model.Tournament;
import com.jvo.competition.service.sync.TournamentService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static com.jvo.competition.commons.Constants.WebConstants.TOURNAMENT_URI;


@RestController
@RequestMapping(value = TOURNAMENT_URI)
public class TournamentController {

  private TournamentService tournamentService;
  private final ModelMapper modelMapper;

  @Autowired
  public TournamentController(TournamentService tournamentService, ModelMapper modelMapper) {
    this.tournamentService = tournamentService;
    this.modelMapper = modelMapper;
  }

  /**
   * Receiving set of tournaments.
   *
   * @return - ResponseEntity with ok status and set of TournamentDto
   */
  @ApiOperation(value = "Get list of tournaments")
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<TournamentDto>> getAllTournaments() {
    return ResponseEntity.ok(tournamentService.getAll());
  }

  /**
   * Finding tournament by id.
   *
   * @param id - tournament identifier
   * @return - ResponseEntity with ok status and TournamentDto object
   */
  @ApiOperation(value = "Find tournament by id")
  @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getTournamentById(@PathVariable Long id) {
    TournamentDto tournamentDto = tournamentService.getById(id).orElseThrow(() -> new ApiResponseException(
        ErrorMessage.of(HttpStatus.NOT_FOUND, String.format("Tournament with id %s not found!", id))));
    return ResponseEntity.ok(tournamentDto);
  }

  /**
   * Creating new tournament.
   *
   * @param tournamentRequest - data for creating new tournament
   * @return - ResponseEntity with create object and new URI
   */
  @ApiOperation(value = "Create new tournament")
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<TournamentDto> addTournament(@RequestBody @Validated TournamentRequest tournamentRequest) {
    Tournament tournament = modelMapper.map(tournamentRequest, Tournament.class);
    TournamentDto saved = tournamentService.add(tournament);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
        .path("/{id}").buildAndExpand(saved.getId()).toUri();
    return ResponseEntity.created(location).body(saved);
  }

  /**
   * Updating existing tournament.
   *
   * @param tournamentRequest - object with new data for tournament
   * @return - ResponseEntity with ok status and tournamentDto object with updated data
   */
  @ApiOperation(value = "Update exiting tournament")
  @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.
      APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> updateTournament(@PathVariable Long id, @RequestBody @Validated TournamentRequest tournamentRequest) {
    Tournament tournament = modelMapper.map(tournamentRequest, Tournament.class);
    return ResponseEntity.ok(tournamentService.update(id, tournament));
  }

  /**
   * Deleting tournament by id.
   *
   * @param id - tournament identifier for deleting
   * @return - ResponseEntity with ok status
   */
  @ApiOperation(value = "Delete tournament")
  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.
      APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> deleteTournament(@PathVariable Long id) {
    tournamentService.delete(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
