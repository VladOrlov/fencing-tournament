package com.jvo.competition.model2;

import com.neovisionaries.i18n.CountryCode;

public class Venue extends PersistableEntity {

    private String name;

    private Float latitude;

    private Float longitude;

    private CountryCode country;

}