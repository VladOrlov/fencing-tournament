package com.jvo.competition.model2;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Getter
@Setter
@SuperBuilder(builderMethodName = "of", buildMethodName = "create")
@EqualsAndHashCode(exclude = "id")
@ToString(exclude = "id")
abstract public class PersistableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    @Column
    private Long version;
}
