package com.jvo.competition.model2;

//Individual that participating in Competition

public class Participant extends PersistableEntity {

    private Competition competition;

    private Individual individual;

    private Integer order;

    //Active or Inactive
    private String status;

    //Athlete or Coach
    private String function;
}
