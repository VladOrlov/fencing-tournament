package com.jvo.competition.model2;

import javax.persistence.Column;

public class Discipline extends PersistableEntity {

    @Column(name = "title")
    private String title;

    private Sport sport;
}
