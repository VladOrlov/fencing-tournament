package com.jvo.competition.model2;

import com.neovisionaries.i18n.CountryCode;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Getter
@Setter
@SuperBuilder(builderMethodName = "of", buildMethodName = "create")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "organisation")
public class Organisation extends PersistableEntity {

    @Column(nullable = false)
    private String name;

    @Column(name = "organisation_type")
    private String organisationType;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "country_code")
    private CountryCode countryCode;

    @Column(name = "country_name")
    private String countryName;


    private Organisation parentOrganisation;

}
