package com.jvo.competition.model2;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Getter
@Setter
@SuperBuilder(builderMethodName = "of", buildMethodName = "create")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "competition")
public class Competition extends PersistableEntity {

    @Column
    private String title;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "finish_date")
    private LocalDate finishDate;

    @Column(name = "organisation")
    private Organisation organisation;

    @Column(name = "venue")
    private Venue venue;

    @Column(name = "logo")
    private String logo;

    @Column(name = "website")
    private String website;

    @Column(name = "travel_information")
    private String travelInformation;

}
