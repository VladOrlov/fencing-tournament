package com.jvo.competition.model2;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@SuperBuilder(builderMethodName = "of", buildMethodName = "create")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"organisation"}, callSuper = true)
@ToString(exclude = {"organisations"}, callSuper = true)
@Entity
@Table(name = "individual")
public class Individual extends PersistableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "profile_images")
    private String profileImage;

    private String nickname;

    private String gender;

    private String nationality;

    private List<String> organisations;

//    @ManyToOne
//    @JoinColumn(name = "organisation_fk", referencedColumnName = "id",
//            foreignKey = @ForeignKey(name = "organisation_foreign_key", value = ConstraintMode.CONSTRAINT))
//    private Organisation organisation;

}

