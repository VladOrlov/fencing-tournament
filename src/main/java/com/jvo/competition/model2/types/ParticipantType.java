package com.jvo.competition.model2.types;


import static com.jvo.competition.model2.types.constants.ParticipantTypeConstants.INDIVIDUAL_ENTITY_NAME;
import static com.jvo.competition.model2.types.constants.ParticipantTypeConstants.TEAM_ENTITY_NAME;

public enum ParticipantType implements WithValueAndDisplayName {
    INDIVIDUAL(INDIVIDUAL_ENTITY_NAME, "Individual"), TEAM(TEAM_ENTITY_NAME, "Team");

    private String value;
    private String displayName;

    ParticipantType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }
}
