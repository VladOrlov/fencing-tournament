package com.jvo.competition.model2.types;

import com.ocs.sdw.model.metadata.WithValueAndDisplayName;

public enum StageType implements WithValueAndDisplayName {
    BRACKETS("BRACKETS", "Brackets"),
    POOLS("POOLS", "Pools"),
    CUMULATIVE("CUMULATIVE", "Cumulative"),
    ELIMINATION("ELIMINATION", "Elimination");

    private String value;
    private String displayName;

    StageType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }
}
