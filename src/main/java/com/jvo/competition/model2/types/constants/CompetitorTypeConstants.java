package com.jvo.competition.model2.types.constants;

public class CompetitorTypeConstants {
    public static final String COMPETITOR_ENTITY_NAME = "COMPETITOR";
    public static final String TEAM_COMPETITOR_ENTITY_NAME = "TEAM_COMPETITOR";
}
