package com.jvo.competition.model2.types;


import static com.jvo.competition.model2.types.constants.CompetitorTypeConstants.COMPETITOR_ENTITY_NAME;
import static com.jvo.competition.model2.types.constants.CompetitorTypeConstants.TEAM_COMPETITOR_ENTITY_NAME;

public enum CompetitorType implements WithValueAndDisplayName {
    COMPETITOR(COMPETITOR_ENTITY_NAME, "Competitor"),
    TEAM_COMPETITOR(TEAM_COMPETITOR_ENTITY_NAME, "Team competitor");

    private String value;
    private String displayName;

    CompetitorType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }
}
