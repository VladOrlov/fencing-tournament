package com.jvo.competition.model2.types.constants;

public class ParticipantTypeConstants {
    public static final String INDIVIDUAL_ENTITY_NAME = "INDIVIDUAL";
    public static final String TEAM_ENTITY_NAME = "TEAM";
}
