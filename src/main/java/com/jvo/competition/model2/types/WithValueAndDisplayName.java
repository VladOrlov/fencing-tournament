package com.jvo.competition.model2.types;

public interface WithValueAndDisplayName {
    String getValue();

    String getDisplayName();
}
