package com.jvo.competition.model2.types;


import static com.jvo.competition.model2.types.constants.ResultTypeConstants.*;

public enum ResultType implements WithValueAndDisplayName {
    RESULT(RESULT_ENTITY_NAME, "Result"),
    CUMULATIVE_RESULT(CUMULATIVE_RESULT_ENTITY_NAME, "Cumulative result"),
    POOL_STANDING(POOL_STANDING_ENTITY_NAME, "Pool standing"),
    PHASE_RESULT(PHASE_RESULT_ENTITY_NAME, "Phase result"),
    STRUCTURED_RESULT(STRUCTURED_RESULT_ENTITY_NAME, "Structured result"),
    FRAME_RESULT(FRAME_RESULT_ENTITY_NAME, "Frame result");

    private String value;
    private String displayName;

    ResultType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }
}
