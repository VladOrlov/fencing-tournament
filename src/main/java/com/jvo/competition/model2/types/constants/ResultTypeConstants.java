package com.jvo.competition.model2.types.constants;

public class ResultTypeConstants {
    public static final String RESULT_ENTITY_NAME = "RESULT";
    public static final String CUMULATIVE_RESULT_ENTITY_NAME = "CUMULATIVE_RESULT";
    public static final String POOL_STANDING_ENTITY_NAME = "POOL_STANDING";
    public static final String PHASE_RESULT_ENTITY_NAME = "PHASE_RESULT";
    public static final String STRUCTURED_RESULT_ENTITY_NAME = "STRUCTURED_RESULT";
    public static final String FRAME_RESULT_ENTITY_NAME = "FRAME_RESULT";
}
