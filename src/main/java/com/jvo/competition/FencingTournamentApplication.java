package com.jvo.competition;

import com.jvo.competition.utility.DatabaseFiller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class FencingTournamentApplication {

  /**
   * Main method.
   */
  public static void main(String[] args) {
    ConfigurableApplicationContext applicationContext = SpringApplication.run(FencingTournamentApplication.class, args);
    DatabaseFiller databaseFiller = applicationContext.getBean(DatabaseFiller.class);
    databaseFiller.fillFencingGuilds();
  }
}
