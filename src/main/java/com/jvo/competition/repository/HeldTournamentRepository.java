package com.jvo.competition.repository;

import com.jvo.competition.model.HeldTournament;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface HeldTournamentRepository extends JpaRepository<HeldTournament, Long> {

  Set<HeldTournament> findAllByTournament(Long id);

}
