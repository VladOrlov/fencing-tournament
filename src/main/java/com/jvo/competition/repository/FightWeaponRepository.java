package com.jvo.competition.repository;

import com.jvo.competition.model.FightWeapon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FightWeaponRepository extends JpaRepository<FightWeapon, Long> {

}