package com.jvo.competition.repository;

import com.jvo.competition.model.HeldTournamentFightScheme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HeldTournamentFightSchemeRepository extends JpaRepository <HeldTournamentFightScheme, Long> {
}
