package com.jvo.competition.repository;

import com.jvo.competition.model.FencingGuild;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

@Repository
public interface FencingGuildRepository extends JpaRepository<FencingGuild, Long> {

  @EntityGraph(value = FencingGuild.FENCING_GUILD_WITH_FIGHTERS, type = EntityGraph.EntityGraphType.FETCH)
  @Query("select fg from FencingGuild fg")
  List<FencingGuild> findAllWithFighters();

  @Query("select fg from FencingGuild fg")
  Stream<FencingGuild> streamAllFencingGuilds();

  @Async
  CompletableFuture<List<FencingGuild>> readAllBy();

  @Async
  Callable<List<FencingGuild>> findAllBy();
}
