package com.jvo.competition.repository;

import com.jvo.competition.model.LeagueLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeagueLevelRepository extends JpaRepository <LeagueLevel, Long> {

}
