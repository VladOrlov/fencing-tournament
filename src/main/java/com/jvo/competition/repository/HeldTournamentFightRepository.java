package com.jvo.competition.repository;

import com.jvo.competition.model.HeldTournamentFight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HeldTournamentFightRepository extends JpaRepository <HeldTournamentFight, Long> {
}
