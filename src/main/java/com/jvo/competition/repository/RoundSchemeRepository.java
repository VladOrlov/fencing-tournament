package com.jvo.competition.repository;

import com.jvo.competition.model.RoundScheme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoundSchemeRepository extends JpaRepository<RoundScheme, Long> {
}
