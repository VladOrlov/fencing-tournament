package com.jvo.competition.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ErrorMessage {

  private int httpStatus;
  private String message;

  private ErrorMessage(int httpStatus, String message) {
    this.httpStatus = httpStatus;
    this.message = message;
  }

  public static ErrorMessage of(int httpStatus, String message) {
    return new ErrorMessage(httpStatus, message);
  }

  public static ErrorMessage of(HttpStatus httpStatus, String message) {
    return new ErrorMessage(httpStatus.value(), message);
  }

  public static ErrorMessage of(int httpStatus) {
    return new ErrorMessage(httpStatus, null);
  }

  public static ErrorMessage of(HttpStatus httpStatus) {
    return new ErrorMessage(httpStatus.value(), null);
  }
}
