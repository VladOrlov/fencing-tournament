package com.jvo.competition.exception;

public class EventNotFoundException extends RuntimeException {

  public EventNotFoundException() {
    super("Tournament not found");
  }

  public EventNotFoundException(String msg) {
    super(msg);
  }

  public EventNotFoundException(String msg, Throwable cause) {
    super(msg, cause);
  }

  public EventNotFoundException(Throwable cause) {
    super(cause);
  }
}
