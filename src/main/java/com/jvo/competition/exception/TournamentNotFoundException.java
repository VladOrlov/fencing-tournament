package com.jvo.competition.exception;

public class TournamentNotFoundException extends RuntimeException {

  public TournamentNotFoundException() {
    super("Tournament not found");
  }

  public TournamentNotFoundException(String msg) {
    super(msg);
  }

  public TournamentNotFoundException(String msg, Throwable cause) {
    super(msg, cause);
  }

  public TournamentNotFoundException(Throwable cause) {
    super(cause);
  }
}

