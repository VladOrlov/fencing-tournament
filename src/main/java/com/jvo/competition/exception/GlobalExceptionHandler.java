package com.jvo.competition.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Locale;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  private final Locale locale;
  private final MessageSource messages;

  @Autowired
  public GlobalExceptionHandler(Locale locale, MessageSource messages) {
    this.locale = locale;
    this.messages = messages;
  }

  private static void logControllerException(Exception cause) {
    log.error("Exception happen in controller", cause);
  }

  /**
   * Handles {@link ApiResponseException} and return appropriate client response.
   *
   * @param cause - {@link ApiResponseException} to handle
   * @return - response with appropriate client message
   */
  @ExceptionHandler(ApiResponseException.class)
  public ResponseEntity<ErrorMessage> handleApiException(ApiResponseException cause) {
    logControllerException(cause);
    ErrorMessage errorMessage = cause.getErrorMessage();
    log.trace("Received of message: {}", errorMessage);
    ResponseEntity<ErrorMessage> responseEntity;
    if (nonNull(errorMessage)) {
      responseEntity = new ResponseEntity<>(errorMessage, HttpStatus.valueOf(errorMessage.getHttpStatus()));
    } else {
      responseEntity = unexpectedErrorResponse();
    }
    return responseEntity;
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<Void> handleConstraintViolationException(ConstraintViolationException cause) {
    logControllerException(cause);
    return ResponseEntity.badRequest().build();
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorMessage> handleException(Exception cause) {
    logControllerException(cause);
    return unexpectedErrorResponse();
  }

  @ExceptionHandler(TournamentNotFoundException.class)
  public ResponseEntity<Object> tournamentNotFoundException(TournamentNotFoundException exception) {
    log.error(exception.getClass().getName(), exception);
    return new ResponseEntity<>(ErrorMessage.of(ErrorStatusCodes.NOT_FOUND.numValue,
        getLocalizedMessage("tournament_not_found")), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(EventNotFoundException.class)
  public ResponseEntity<Object> eventNotFoundException(EventNotFoundException exception) {
    log.error(exception.getClass().getName(), exception);
    return new ResponseEntity<>(ErrorMessage.of(ErrorStatusCodes.NOT_FOUND.numValue,
        getLocalizedMessage("event_not_found")), HttpStatus.NOT_FOUND);
  }


  private enum ErrorStatusCodes {
    BAD_REQUEST(400),
    NOT_FOUND(404),
    SERVICE_UNAVAILABLE(503);

    private int numValue;

    ErrorStatusCodes(int numValue) {
      this.numValue = numValue;
    }
  }

  private ResponseEntity<ErrorMessage> unexpectedErrorResponse() {
    ErrorMessage serverErrorMessage = ErrorMessage.of(INTERNAL_SERVER_ERROR.value(), "Internal server of");
    return new ResponseEntity<>(serverErrorMessage, INTERNAL_SERVER_ERROR);
  }

  private String getLocalizedMessage(String key) {
    return Optional.ofNullable(messages.getMessage(key, null, LocaleContextHolder.getLocale()))
        .orElseGet(() -> messages.getMessage(key, null, locale));
  }
}
