package com.jvo.competition.exception;

import lombok.Getter;

public class ApiResponseException extends RuntimeException {

  @Getter
  private ErrorMessage errorMessage;

  public ApiResponseException(ErrorMessage errorMessage) {
    super(errorMessage.getMessage());
    this.errorMessage = errorMessage;
  }

  public ApiResponseException(ErrorMessage errorMessage, Throwable cause) {
    super(errorMessage.getMessage(), cause);
    this.errorMessage = errorMessage;
  }
}
