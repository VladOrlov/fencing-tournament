package com.jvo.competition.service.async;

import org.springframework.scheduling.annotation.Async;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Stream;


public interface AsyncCrudService<T> {

  default Mono<T> getMonoFromCallable(Callable<T> callable, Scheduler scheduler) {
    return Mono.fromCallable(callable).publishOn(scheduler);
  }

  default Flux<T> getFluxFromStream(Stream<T> stream, Scheduler scheduler) {
    return Flux.fromStream(stream).publishOn(scheduler);
  }

  @Async
  List<T> findAll();

  @Async
  T save(T entity);

  @Async
  T find(String id);

  Mono<T> findAllWithMono();

  Flux<T> findAllWithFlux();

}
