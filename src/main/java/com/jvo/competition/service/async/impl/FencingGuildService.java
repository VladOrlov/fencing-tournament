//package com.pss.fencing.tournament.service.async.impl;
//
//import com.pss.fencing.tournament.model.FencingGuild;
//import com.pss.fencing.tournament.repository.FencingGuildRepository;
//import com.pss.fencing.tournament.service.async.AsyncCrudService;
//
//import lombok.extern.slf4j.Slf4j;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.support.TransactionTemplate;
//
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;
//import reactor.core.scheduler.Scheduler;
//
//import java.util.List;
//import java.util.concurrent.Callable;
//
//@Slf4j
//@Service
//public class FencingGuildService implements AsyncCrudService<FencingGuild> {
//
//  @Autowired
//  private TransactionTemplate transactionTemplate;
//
//  @Autowired
//  @Qualifier("jdbcScheduler")
//  private Scheduler jdbcScheduler;
//
//  @Autowired
//  private FencingGuildRepository guildRepository;
//
//  @Override
//  public List<FencingGuild> findAll() {
//    return null;
//  }
//
//  @Override
//  public FencingGuild save(FencingGuild entity) {
//    return null;
//  }
//
//  @Override
//  public FencingGuild find(String id) {
//    return null;
//  }
//
//  @Override
//  public Mono<FencingGuild> findAllWithMono() {
//    return null;
//  }
//
//  @Override
//  public Flux<FencingGuild> findAllWithFlux() {
//    return null;
//  }
//
//  public Mono<Iterable<FencingGuild>> findAllAsync() {
//    return async(() -> guildRepository.findAll());
//  }
//
//  private <T> Mono<T> async(Callable<T> callable) {
//    return Mono.fromCallable(callable).publishOn(jdbcScheduler);
//  }
//}
