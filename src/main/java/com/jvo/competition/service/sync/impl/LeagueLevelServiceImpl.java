package com.jvo.competition.service.sync.impl;

import com.jvo.competition.service.sync.LeagueLevelService;
import com.jvo.competition.dto.LeagueLevelDto;
import com.jvo.competition.dto.request.LeagueLevelRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class LeagueLevelServiceImpl implements LeagueLevelService {

  @Override
  public List<LeagueLevelDto> getAll() {
    return null;
  }

  @Override
  public Optional<LeagueLevelDto> getById(Long id) {
    return Optional.empty();
  }

  @Override
  public LeagueLevelDto add(LeagueLevelRequest leagueLevel) {
    return null;
  }

  @Override
  public LeagueLevelDto update(LeagueLevelRequest leagueLevel) {
    return null;
  }

  @Override
  public void delete(Long id) {

  }

  @Override
  public boolean isExist(Long id) {
    return false;
  }

  @Override
  public boolean notExist(Long id) {
    return false;
  }
}
