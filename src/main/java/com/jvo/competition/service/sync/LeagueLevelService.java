package com.jvo.competition.service.sync;

import com.jvo.competition.dto.LeagueLevelDto;
import com.jvo.competition.dto.request.LeagueLevelRequest;

import java.util.List;
import java.util.Optional;

public interface LeagueLevelService {

  List<LeagueLevelDto> getAll();

  Optional<LeagueLevelDto> getById(Long id);

  LeagueLevelDto add(LeagueLevelRequest leagueLevel);

  LeagueLevelDto update(LeagueLevelRequest leagueLevel);

  void delete(Long id);

  boolean isExist(Long id);

  boolean notExist(Long id);

}
