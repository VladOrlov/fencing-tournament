package com.jvo.competition.service.sync;

import com.jvo.competition.dto.FencingGuildDto;
import com.jvo.competition.model.FencingGuild;

import java.util.List;
import java.util.Optional;

public interface FencingGuildService {

  List<FencingGuildDto> getAll();

  Optional<FencingGuildDto> getById(Long id);

  FencingGuildDto add(FencingGuild fencingGuild);

  FencingGuildDto update(FencingGuild fencingGuild);

  void delete(Long id);

  boolean isExist(Long id);

  boolean notExist(Long id);

}
