package com.jvo.competition.service.sync.impl;

import com.google.common.collect.Lists;
import com.jvo.competition.exception.TournamentNotFoundException;
import com.jvo.competition.service.sync.TournamentService;
import com.jvo.competition.utility.ModelMapperTypes;
import com.jvo.competition.dto.TournamentDto;
import com.jvo.competition.model.Tournament;
import com.jvo.competition.repository.TournamentRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TournamentServiceImpl implements TournamentService {

  private TournamentRepository tournamentRepository;
  private ModelMapper modelMapper;


  @Autowired
  public TournamentServiceImpl(TournamentRepository tournamentRepository, ModelMapper modelMapper) {
    this.tournamentRepository = tournamentRepository;
    this.modelMapper = modelMapper;
  }

  /**
   * Get set of tournaments.
   *
   * @return - Set of TournamentDto
   */
  @Override
  public List<TournamentDto> getAll() {
    List<Tournament> tournaments = tournamentRepository.findAll();
    if (tournaments.isEmpty()) {
      return Lists.newArrayList();
    } else {
      return modelMapper.map(tournaments, ModelMapperTypes.tournamentsSetType());
    }
  }

  /**
   * Find tournament by id.
   *
   * @param id - tournament's identifier
   * @return - Optional of Tournament object
   */
  @Override
  public Optional<TournamentDto> getById(Long id) {
    Optional<Tournament> optionalTournament = tournamentRepository.findById(id);
    return optionalTournament.map(tournament ->
        modelMapper.map(tournament, TournamentDto.class));
    //    throw new TournamentNotFoundException(String.format("Tournament with id %s was not found in database!", id));
  }

  /**
   * Add new tournament.
   *
   * @param tournament - tournament object for saving
   * @return - saved object
   */
  @Override
  public TournamentDto add(Tournament tournament) {
    return modelMapper.map(tournamentRepository.save(tournament), TournamentDto.class);
  }

  /**
   * Update existing tournament.
   *
   * @param tournament - new data for updating
   * @return - updated object
   */
  @Override
  public TournamentDto update(Long id, Tournament tournament) {
    Optional<Tournament> existingTournament = tournamentRepository.findById(id);
    if (existingTournament.isPresent()) {
      Tournament updatedTournament = existingTournament.get();
      updatedTournament.setName(tournament.getName());
      updatedTournament.setCountryCode(tournament.getCountryCode());
      updatedTournament.setCountryName(tournament.getCountryName());
      updatedTournament.setDescription(tournament.getDescription());
      return modelMapper.map(tournamentRepository.save(updatedTournament), TournamentDto.class);
    } else {
      throw new TournamentNotFoundException("Tournament not found");
    }
  }

  /**
   * Delete tournament by id.
   *
   * @param id - tournament identifier
   */
  @Override
  public void delete(Long id) {
    try {
      tournamentRepository.deleteById(id);
    } catch (Exception e) {
      log.error("Tournament with this id not found");
      throw new TournamentNotFoundException("Tournament with this id not found");
    }
  }

  /**
   * Check for tournament's existing.
   *
   * @param id - tournament's identifier
   * @return - boolean value
   */
  @Override
  public boolean isExist(Long id) {
    return tournamentRepository.existsById(id);
  }

  /**
   * Check for tournament's not existing.
   *
   * @param id - tournament's identifier
   * @return - boolean value
   */
  @Override
  public boolean notExist(Long id) {
    return !tournamentRepository.existsById(id);
  }
}
