package com.jvo.competition.service.sync;

import com.jvo.competition.dto.LeagueDto;
import com.jvo.competition.dto.request.LeagueRequest;

import java.util.List;
import java.util.Optional;

public interface LeagueService {

  List<LeagueDto> getAll();

  Optional<LeagueDto> getById(Long id);

  LeagueDto add(LeagueRequest league);

  LeagueDto update(LeagueRequest league);

  void delete(Long id);

  boolean isExist(Long id);

  boolean notExist(Long id);

}
