package com.jvo.competition.service.sync.impl;

import com.jvo.competition.dto.LeagueDto;
import com.jvo.competition.dto.request.LeagueRequest;
import com.jvo.competition.model.FencingGuild;
import com.jvo.competition.model.League;
import com.jvo.competition.repository.FencingGuildRepository;
import com.jvo.competition.repository.LeagueRepository;
import com.jvo.competition.service.sync.LeagueService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class LeagueServiceImpl implements LeagueService {

    private final LeagueRepository leagueRepository;

    private final ModelMapper modelMapper;

    private final FencingGuildRepository fencingGuildRepository;

    @Autowired
    public LeagueServiceImpl(LeagueRepository leagueRepository, ModelMapper modelMapper, FencingGuildRepository fencingGuildRepository) {
        this.leagueRepository = leagueRepository;
        this.modelMapper = modelMapper;
        this.fencingGuildRepository = fencingGuildRepository;
    }

    @Override
    public List<LeagueDto> getAll() {
        return null;
    }

    @Override
    public Optional<LeagueDto> getById(Long id) {

        League league = leagueRepository.getOne(id);

        return Optional.empty();
    }

    @Override
    public LeagueDto add(LeagueRequest leagueRequest) {
        if (Objects.nonNull(leagueRequest.getLevel())) {
            FencingGuild one = fencingGuildRepository.getOne(leagueRequest.getLevel());
        }


        return null;
    }

    @Override
    public LeagueDto update(LeagueRequest leagueRequest) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public boolean isExist(Long id) {
        return false;
    }

    @Override
    public boolean notExist(Long id) {
        return false;
    }
}
