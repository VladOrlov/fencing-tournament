package com.jvo.competition.service.sync;

import com.jvo.competition.dto.FighterDto;
import com.jvo.competition.model.Fighter;

import java.util.List;
import java.util.Optional;

public interface FighterService {

  List<FighterDto> getAll();

  Optional<FighterDto> getById(Long id);

  FighterDto add(Fighter fighter);

  FighterDto update(Fighter fighter);

  void delete(Long id);

  boolean isExist(Long id);

  boolean notExist(Long id);
}
