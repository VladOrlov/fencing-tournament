package com.jvo.competition.service.sync.impl;

import com.jvo.competition.exception.EventNotFoundException;
import com.jvo.competition.service.sync.HeldTournamentService;
import com.jvo.competition.utility.ModelMapperTypes;
import com.jvo.competition.dto.HeldTournamentDto;
import com.jvo.competition.model.HeldTournament;
import com.jvo.competition.repository.HeldTournamentRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class HeldTournamentServiceImpl implements HeldTournamentService {

  private HeldTournamentRepository heldTournamentRepository;
  private ModelMapper modelMapper;

  @Autowired
  public HeldTournamentServiceImpl(HeldTournamentRepository heldTournamentRepository, ModelMapper modelMapper) {
    this.heldTournamentRepository = heldTournamentRepository;
    this.modelMapper = modelMapper;
  }

  @Override
  public Set<HeldTournamentDto> getAll() {
    Set<HeldTournament> events = new HashSet<>(heldTournamentRepository.findAll());
    if (events.isEmpty()) {
      return Collections.emptySet();
    } else {
      return modelMapper.map(events, ModelMapperTypes.heldTournamentsSetType());
    }
  }

  @Override
  public Set<HeldTournamentDto> getAllForTournament(long tournamentId) {
    Set<HeldTournament> events = heldTournamentRepository.findAllByTournament(tournamentId);
    if (events.isEmpty()) {
      return Collections.emptySet();
    } else {
      return modelMapper.map(events, ModelMapperTypes.heldTournamentsSetType());
    }
  }

  @Override
  public Optional<HeldTournamentDto> getById(Long id) {
    Optional<HeldTournament> event = heldTournamentRepository.findById(id);
    if (event.isPresent()) {
      return modelMapper.map(event, ModelMapperTypes.heldTournamentsSetType());
    } else {
      throw new EventNotFoundException("Event with this id was not found in database");
    }
  }

  @Override
  public HeldTournamentDto add(HeldTournament tournament) {
    return modelMapper.map(heldTournamentRepository.save(tournament), HeldTournamentDto.class);
  }

  @Override
  public HeldTournamentDto update(Long id, HeldTournament heldTournament) {
    Optional<HeldTournament> existingEvent = heldTournamentRepository.findById(id);
    if (existingEvent.isPresent()) {
      HeldTournament updatedEvent = existingEvent.get();
      updatedEvent.setTournament(heldTournament.getTournament());
      updatedEvent.setStartDate(heldTournament.getStartDate());
      updatedEvent.setEndDate(heldTournament.getEndDate());
      updatedEvent.setAddress(heldTournament.getAddress());
      return modelMapper.map(heldTournamentRepository.save(updatedEvent), HeldTournamentDto.class);
    } else {
      throw new EventNotFoundException("Event not found in database");
    }
  }

  @Override
  public void delete(Long id) {
    try {
      heldTournamentRepository.deleteById(id);
    } catch (Exception e) {
      log.error("Event with this id not found");
      throw new EventNotFoundException("Event with this id not found");
    }
  }

  @Override
  public boolean isExist(Long id) {
    return heldTournamentRepository.existsById(id);
  }

  @Override
  public boolean notExist(Long id) {
    return !heldTournamentRepository.existsById(id);
  }


}
