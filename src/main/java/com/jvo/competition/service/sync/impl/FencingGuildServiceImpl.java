package com.jvo.competition.service.sync.impl;

import com.jvo.competition.service.sync.FencingGuildService;
import com.jvo.competition.utility.ModelMapperTypes;
import com.jvo.competition.dto.FencingGuildDto;
import com.jvo.competition.model.FencingGuild;
import com.jvo.competition.repository.FencingGuildRepository;

import lombok.extern.slf4j.Slf4j;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class FencingGuildServiceImpl implements FencingGuildService {

  private final FencingGuildRepository fencingGuildRepository;

  private final ModelMapper modelMapper;

  @Autowired
  public FencingGuildServiceImpl(FencingGuildRepository fencingGuildRepository, ModelMapper modelMapper) {
    this.fencingGuildRepository = fencingGuildRepository;
    this.modelMapper = modelMapper;
  }

  @Override
  public List<FencingGuildDto> getAll() {
    List<FencingGuild> allGuilds = fencingGuildRepository.findAllWithFighters();
    return modelMapper.map(allGuilds, ModelMapperTypes.fencingGuildsListType());
  }

  @Override
  public Optional<FencingGuildDto> getById(Long id) {
    Optional<FencingGuild> guildOptional = fencingGuildRepository.findById(id);
    return guildOptional.map(fencingGuild -> modelMapper.map(fencingGuild, FencingGuildDto.class));
  }

  @Override
  public FencingGuildDto add(FencingGuild fencingGuild) {
    return modelMapper.map(fencingGuildRepository.save(fencingGuild), FencingGuildDto.class);
  }

  @Transactional
  @Override
  public FencingGuildDto update(FencingGuild fencingGuild) {
    Optional<FencingGuild> existingGuild = fencingGuildRepository.findById(fencingGuild.getId());
    if (existingGuild.isPresent()) {
      FencingGuild updatingGuild = existingGuild.get();
      updatingGuild.setName(fencingGuild.getName());
      updatingGuild.setCountryCode(fencingGuild.getCountryCode());
      updatingGuild.setCountryName(fencingGuild.getCountryName());
      FencingGuild savedGuild = fencingGuildRepository.save(updatingGuild);
      return modelMapper.map(savedGuild, FencingGuildDto.class);
    } else {
      return null;
    }
  }

  @Override
  public void delete(Long id) {
    fencingGuildRepository.deleteById(id);
  }

  @Override
  public boolean isExist(Long id) {
    return fencingGuildRepository.existsById(id);
  }

  @Override
  public boolean notExist(Long id) {
    return !isExist(id);
  }
}
