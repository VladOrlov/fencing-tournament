package com.jvo.competition.service.sync;

import com.jvo.competition.dto.HeldTournamentDto;
import com.jvo.competition.model.HeldTournament;

import java.util.Optional;
import java.util.Set;

/**
 * @author gss
 */
public interface HeldTournamentService {
  Set<HeldTournamentDto> getAll();

  Optional<HeldTournamentDto> getById(Long id);

  HeldTournamentDto add(HeldTournament tournament);

  HeldTournamentDto update(Long id, HeldTournament tournament);

  void delete(Long id);

  boolean isExist(Long id);

  boolean notExist(Long id);

  Set<HeldTournamentDto> getAllForTournament(long tournamentId);
}

