package com.jvo.competition.service.sync.impl;

import com.jvo.competition.service.sync.FighterService;
import com.jvo.competition.dto.FighterDto;
import com.jvo.competition.model.Fighter;
import com.jvo.competition.repository.FighterRepository;
import com.jvo.competition.utility.ModelMapperTypes;

import lombok.extern.slf4j.Slf4j;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class FighterServiceImpl implements FighterService {

  private final FighterRepository fighterRepository;

  private final ModelMapper modelMapper;

  @Autowired
  public FighterServiceImpl(FighterRepository fighterRepository, ModelMapper modelMapper) {
    this.fighterRepository = fighterRepository;
    this.modelMapper = modelMapper;
  }

  @Override
  public List<FighterDto> getAll() {
    List<Fighter> fighters = fighterRepository.findAll();
    return modelMapper.map(fighters, ModelMapperTypes.fighterListType());
  }

  @Override
  public Optional<FighterDto> getById(Long id) {
    Optional<Fighter> fighterOptional = fighterRepository.findById(id);
    return fighterOptional.map(fighter -> modelMapper.map(fighter, FighterDto.class));
  }

  @Override
  public FighterDto add(Fighter fighter) {
    return modelMapper.map(fighterRepository.save(fighter), FighterDto.class);
  }

  @Override
  public FighterDto update(Fighter fighter) {
    Optional<Fighter> existingFighter = fighterRepository.findById(fighter.getId());
    if (existingFighter.isPresent()) {
      Fighter updatingFighter = existingFighter.get();
      updatingFighter.setFirstName(fighter.getFirstName());
      updatingFighter.setLastName(fighter.getLastName());
      updatingFighter.setAge(fighter.getAge());
      updatingFighter.setGuild(fighter.getGuild());
      return modelMapper.map(fighterRepository.save(updatingFighter), FighterDto.class);
    } else {
      return null;
    }
  }

  @Override
  public void delete(Long id) {
    fighterRepository.deleteById(id);
  }

  @Override
  public boolean isExist(Long id) {
    return fighterRepository.existsById(id);
  }

  @Override
  public boolean notExist(Long id) {
    return !isExist(id);
  }
}
