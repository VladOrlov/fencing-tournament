package com.jvo.competition.service.sync;

import com.jvo.competition.dto.TournamentDto;
import com.jvo.competition.model.Tournament;

import java.util.List;
import java.util.Optional;

public interface TournamentService {

  List<TournamentDto> getAll();

  Optional<TournamentDto> getById(Long id);

  TournamentDto add(Tournament tournament);

  TournamentDto update(Long id, Tournament tournament);

  void delete(Long id);

  boolean isExist(Long id);

  boolean notExist(Long id);
}
