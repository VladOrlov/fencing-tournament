package com.jvo.competition.model;

import com.neovisionaries.i18n.CountryCode;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Set;


@Getter
@Setter
@Builder(builderMethodName = "of", buildMethodName = "create")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id", "fighters"}, callSuper = true)
@ToString(exclude = {"id", "fighters"})
@DynamicUpdate
@Entity
@Table(name = "fencing_guild")
@NamedEntityGraphs({@NamedEntityGraph(name = "FencingGuild.withFighters", attributeNodes = {@NamedAttributeNode("fighters")})})
public class FencingGuild extends AbstractEntity {

  public static final String FENCING_GUILD_WITH_FIGHTERS = "FencingGuild.withFighters";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(nullable = false)
  private String name;

  @Enumerated(value = EnumType.STRING)
  @Column(name = "country_code", nullable = false)
  private CountryCode countryCode;

  @Column(name = "country_name", nullable = false)
  private String countryName;

  @Singular
  @OneToMany(mappedBy = "guild", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<Fighter> fighters;

}
