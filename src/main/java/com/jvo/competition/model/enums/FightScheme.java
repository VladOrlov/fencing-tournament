package com.jvo.competition.model.enums;

public enum FightScheme {

    ALL_VS_ALL, NUMBER_OF_FIGHTS, DROP_OUT

}
