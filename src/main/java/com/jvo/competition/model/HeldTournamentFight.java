package com.jvo.competition.model;

import com.jvo.competition.model.enums.FightResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "id")
@ToString(exclude = "id")
@Entity
@Table(name = "held_tournament_fight")
public class HeldTournamentFight {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "fight_number", nullable = false)
  private Long fightNumber;

  @ManyToOne
  @JoinColumn(name = "held_tournament_fk", referencedColumnName = "id",
      foreignKey = @ForeignKey(name = "held_tournament_foreign_key", value = ConstraintMode.CONSTRAINT))
  private HeldTournament heldTournament;

  @Column(nullable = false)
  private Integer stadium;

  @ManyToOne
  @JoinColumn(name = "first_fighter_fk", referencedColumnName = "id",
      foreignKey = @ForeignKey(name = "first_fighter_foreign_key", value = ConstraintMode.CONSTRAINT))
  private Fighter firstFighter;

  @ManyToOne
  @JoinColumn(name = "second_fighter_fk", referencedColumnName = "id",
      foreignKey = @ForeignKey(name = "second_fighter_foreign_key", value = ConstraintMode.CONSTRAINT))
  private Fighter secondFighter;

  @Column
  private Integer firstFighterScore;

  @Column
  private Integer secondFighterScore;

  @ManyToOne
  @JoinColumn(name = "fight_weapon_fk", referencedColumnName = "id",
      foreignKey = @ForeignKey(name = "fight_weapon_foreign_key", value = ConstraintMode.CONSTRAINT))
  private FightWeapon weapon;

  @Column
  private LocalDateTime date;

  @Enumerated(value = EnumType.STRING)
  private FightResult result;

}
