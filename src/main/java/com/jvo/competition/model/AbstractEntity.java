package com.jvo.competition.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Version;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
public abstract class AbstractEntity implements Serializable {

  @Column
  @Version
  private long version;

  @Column(name = "created_date", nullable = false, updatable = false)
  @CreatedDate
  private long createdDate;

  @Column(name = "modified_date")
  @LastModifiedDate
  private long modifiedDate;

}
