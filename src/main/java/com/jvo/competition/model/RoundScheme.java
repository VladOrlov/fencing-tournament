package com.jvo.competition.model;

import com.jvo.competition.model.enums.FightScheme;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "id")
@ToString(exclude = "id")
@Entity
@Table(name = "round_scheme")
public class RoundScheme {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", nullable = false)
  private String name;

  @Enumerated
  @Column(name = "fight_scheme", nullable = false)
  private FightScheme fightScheme;

  @Column(name = "number_of_fights", nullable = false)
  private Integer fightsNumber;

  @Column(name = "fights_series")
  private boolean fightsSeries;

  @Column(name = "fights_in_series")
  private Integer fightsInSeries;

  @Column(name = "use_different_weapon_for_series")
  private boolean useDifferentWeapon;

  @Column(name = "number_of_fighters_pass")
  private Integer passingFightersNumber;

}
