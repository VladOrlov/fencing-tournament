package com.jvo.competition.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder(builderMethodName = "of", buildMethodName = "create")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id", "guild", "version"})
@ToString(exclude = {"id", "guild", "version"})
@Entity
@Table(name = "fighter")
public class Fighter {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "first_name", nullable = false)
  private String firstName;

  @Column(name = "last_name", nullable = false)
  private String lastName;

  @Column
  private Integer age;

  @ManyToOne
  @JoinColumn(name = "guild_fk", referencedColumnName = "id",
      foreignKey = @ForeignKey(name = "guild_foreign_key", value = ConstraintMode.CONSTRAINT))
  private FencingGuild guild;

  @Version
  @Column
  private Long version;

}

