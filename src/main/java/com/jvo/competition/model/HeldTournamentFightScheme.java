package com.jvo.competition.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder(builderMethodName = "of", buildMethodName = "create")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
@ToString(exclude = {"id"})
@Entity
@Table(name = "held_tournament_fight_scheme")
public class HeldTournamentFightScheme {

  @Id
  @GeneratedValue
  private Long id;

  @OneToOne
  private HeldTournament heldTournament;

  @ManyToOne
  @JoinColumn(name = "qualifying_scheme_fk", referencedColumnName = "id",
      foreignKey = @ForeignKey(name = "qualifying_foreign_key", value = ConstraintMode.CONSTRAINT))
  private RoundScheme qualifyingRoundScheme;

  @Column
  private Integer passingFighters;

  @ManyToOne
  @JoinColumn(name = "final_round_scheme_fk", referencedColumnName = "id",
      foreignKey = @ForeignKey(name = "final_round_foreign_key", value = ConstraintMode.CONSTRAINT))
  private RoundScheme finalRoundScheme;

}
