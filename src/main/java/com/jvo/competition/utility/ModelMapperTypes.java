package com.jvo.competition.utility;

import com.jvo.competition.dto.FencingGuildDto;
import com.jvo.competition.dto.FighterDto;
import com.jvo.competition.dto.HeldTournamentDto;
import com.jvo.competition.dto.TournamentDto;
import org.modelmapper.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

public class ModelMapperTypes {

  public static Type fencingGuildsListType() {
    return new TypeToken<List<FencingGuildDto>>() {}.getType();
  }

  public static Type fighterListType() {
    return new TypeToken<List<FighterDto>>() {}.getType();
  }

  public static Type tournamentsSetType() {
    return new TypeToken<List<TournamentDto>>() {}.getType();
  }

  public static Type heldTournamentsSetType() {
    return new TypeToken<Set<HeldTournamentDto>>() {}.getType();
  }

}
