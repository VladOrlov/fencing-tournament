package com.jvo.competition.utility;

import com.google.common.collect.Sets;
import com.neovisionaries.i18n.CountryCode;
import com.jvo.competition.model.FencingGuild;
import com.jvo.competition.model.Fighter;
import com.jvo.competition.repository.FencingGuildRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Slf4j
@Service
public class DatabaseFiller {

  @Autowired
  private FencingGuildRepository fencingGuildRepository;

  /**
   * Fill data base.
   */
  public void fillFencingGuilds() {

    FencingGuild duelFencingGuild = FencingGuild.of()
        .name("Guild of Duel Fencing")
        .countryName("Ukraine")
        .countryCode(CountryCode.UA)
        .create();
    duelFencingGuild.setFighters(getGuildOfDuelFencingFighters(duelFencingGuild));

    fencingGuildRepository.save(duelFencingGuild);
  }

  private Set<Fighter> getGuildOfDuelFencingFighters(FencingGuild fencingGuild) {

    Set<Fighter> fighters = Sets.newHashSet();

    fighters.add(Fighter.of()
        .firstName("Ivan")
        .lastName("Sugak-Snarsky")
        .age(35)
        .guild(fencingGuild)
        .create());
    fighters.add(Fighter.of()
        .firstName("Anton")
        .lastName("Fedchenko")
        .age(36)
        .guild(fencingGuild)
        .create());
    fighters.add(Fighter.of()
        .firstName("Stanislav")
        .lastName("Podlipenskiy")
        .age(35)
        .guild(fencingGuild)
        .create());

    return fighters;
  }
}
